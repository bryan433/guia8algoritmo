-Empezando el programa: para inicializar el programa debemos colocar ./programa desde la terminal al cual debemo agregarle dos parametros ./programa 1000 N (debe ser un numero entre [-1000000, 1000000] y el segundo parametro debe ser N o S los cuales tienen un significado, la N solo nos mostrara el tiempo, y el S nos mostrara un arreglo desordenado , con el tiempo), al ingresar estos parametros nuestro programa empezara a realizar algunas tareas, las cuales son, en un arreglo añadir la cantidad de numeros que usted le ingreso en el parametro, luego de eso va a ordenar este arreglo con cada uno de los tipos de ordenamiento, y va a calcular el tiempo que se demorar cada uno para poder saber cual es el mas efectivo a la hora de utilizar alguno.

-Instalacion: Para la isntalacion de este programa ingresar a este repositorio https://gitlab.com/bryan433/guia8algoritmo y clonarlo (git clone Link), luego de eso debemos entrar a la carpeta donde tenemos el archvio clonado, desde esta carpeta debemos abrir la terminal y colocar make, de esta forma solamente debemos empezar el programa.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra
