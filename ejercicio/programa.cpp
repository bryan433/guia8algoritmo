#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>


using namespace std;

#include <unistd.h>



//imprimimos los numeros de la arrays
void imprimir_arrays(int N, int A[]){
	for (int i = 0; i < N; i++){
		cout << "[" << A[i] << "] ";
	}
	cout << endl;
}


void burbujamenor(int N, int A[]){
	int aux;
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	
	for (int i = 0; i < N; i++){
		for (int j = N; j > i; j--){
			if (A[j-1] > A[j]){
				aux = A[j-1];
				A[j-1] = A[j];
				A[j] = aux;
			}
		}
	}
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "Burbuja Menor       | " << tiempo << " milisegundos" << endl;
}


void burbujamayor(int N, int A[]){
	int aux;
	aux = 0;
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	
	for (int i = (N - 2); i >= 0; i--){
		for (int j = 0; j <= i; j++){
			if (A[j] > A[j + 1]){
				aux = A[j];
				A[j] = A[j + 1];
				A[j + 1] = aux;
			}
		}
	}
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "Burbuja mayor       | " << tiempo << " milisegundos" << endl;
}


void insercion (int N, int A[]){
	int aux;
	int k;
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	
	for (int i = 0; i < N; i++){
		aux = A[i];
		k = i - 1;
		while ((k >= 0) && (aux < A[k])){
			A[k+1] = A[k];
			k = k - 1;
		}
		A[k + 1] = aux;
	}
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "insercion           | " << tiempo << " milisegundos" << endl;
}


void insercionbinaria(int N, int A[]){
	int aux;
	int izq;
	int der;
	int m;
	int j;
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	
	for (int i = 1; i < N; i++){
		aux = A[i];
		izq = 0;
		der = (i - 1);
		while (izq <= der){
			m = (int)((der + izq)/2);
			if (aux < A[m]){
				der = m - 1;
			}
			else{
				izq = m + 1;
			}
		}
		j = i - 1;
		while (j >= izq){
			A[j + 1] = A[j];
			j = j -1;
		}
		A[izq] = aux;
	}
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "insercion binaria   | " << tiempo << " milisegundos" << endl;
}


void seleccion(int N, int A[]){
	int menor;
	int k;
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	
	for (int i = 0; i < N - 1; i++){
		menor = A[i];
		k = i;
		for (int j = i + 1; j < N; j++){
			if (A[j] < menor){
				menor = A[j];
				k = j;
			}
		}
		A[k] = A[i];
		A[i] = menor;
	}
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "seleccion           | " << tiempo << " milisegundos" << endl;
}


void shell(int N, int A[]){
	int entero;
	int i;
	int aux;
	bool band;
	double tiempo = 0;
	clock_t comienzo;
	comienzo = clock();
	
	entero = N + 1;
	
	while (entero > 1){
		entero = (int)(entero/2);
		band = true;
		while (band == true){
			band = false;
			i = 0;
			while ((i + entero) <= N){
				if (A[i] > A[i + entero]){
				aux = A[i];
				A[i] = A[i + entero];
				A[i + entero] = aux;
				band = true;
				}
				i = i + 1;
			}
		}
	}
	tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
	cout << "shell               | " << tiempo << " milisegundos" << endl;
	
}

void reduceQuicksort(int A[], int inicio, int fin, int &pos){
    int aux;
    int izq;;
    int der;
    bool bandera;
    
    izq = inicio;
    der = fin;
    bandera = true;
    pos = inicio;

    while(bandera == true){
        while(A[pos] <= A[der] && pos != der){
            der--;
        }

        if(pos == der){
            bandera = false;
        }

        else{
            aux = A[pos];
            A[pos] = A[der];
            A[der] = aux;
            pos = der;

            while(A[pos] >= A[izq] && pos != izq){
                izq++;
                aux = A[pos];
                A[pos] = A[izq];
                A[izq] = aux;
                pos = izq;
            }
        }
    }
}


double quicksort(int N, int A[]){
    int pilaMenor[N];
    int pilaMayor[N];
    int inicio;
    int fin;
    int pos;
    int tope = 0;

    double tiempo;
    clock_t comienzo;
	comienzo = clock();

    pilaMenor[tope] = 0;
    pilaMayor[tope] = N-1;
    
    while(tope >= 0 ){
        inicio = pilaMenor[tope];
        fin = pilaMayor[tope];
        tope--;

        reduceQuicksort(A, inicio, fin, pos);

        if(inicio < (pos-1)){
            tope++;
            pilaMenor[tope] = inicio;
            pilaMayor[tope] = pos-1;
        }

        if(fin > (pos+1)){
            tope++;
            pilaMenor[tope] = pos+1;
            pilaMayor[tope] = fin;
        }
    }

    tiempo = (clock()-comienzo)/(double)CLOCKS_PER_SEC * 1000;
    return tiempo;
}



void MostarTiempo(int aux2[], int aux3[], int aux4[], int aux5[], int aux6[], int aux7[], int aux8[], int N){
	double tiempoQuicksort;
	
	burbujamenor(N, aux2);
	burbujamayor(N, aux3);
	insercion(N, aux4);
	insercionbinaria(N, aux5);
	seleccion(N, aux6);
	shell(N, aux7);
	quicksort(N, aux8);
	
	tiempoQuicksort = quicksort(N, aux8);
	cout << "Quicksort           | " << tiempoQuicksort << " milisegundos" << endl;
	
	
}

int verificarTamano(int N, char *argv[]){
	int numero;
	string ver = argv[2];
	
	if (N >= -1000000 && N <= 1000000){
		int aux1[N];
		int aux2[N];
		int aux3[N];
		int aux4[N];
		int aux5[N];
		int aux6[N];
		int aux7[N];
		int aux8[N];
		
		for(int i = 0; i < N; i++){
			aux1[i] = (rand() %N) + 1;
		}
		for (int j = 0; j < N; j++){
			numero = aux1[j];
			aux2[j] = numero;
			aux3[j] = numero;
			aux4[j] = numero;
			aux5[j] = numero;
			aux6[j] = numero;
			aux7[j] = numero;
			aux8[j] = numero;
		} 
			
		if (ver == "s" or ver == "S"){
			imprimir_arrays(N, aux1);
			MostarTiempo(aux2, aux3, aux4, aux5, aux6, aux7, aux8, N);
		}
		else if (ver == "n" or ver == "N"){
			MostarTiempo(aux2, aux3, aux4, aux5, aux6, aux7, aux8, N);
		}
		else{
			cout << "Debe ingresar s o n en el segundo parametro para ver el arreglo desordenado" << endl;
			cout << "s = muesta el arreglo desordenado y el tiempo de ordenamiento" << endl;
			cout << "n solo muesta el tiempo de ordenamiento" << endl;
			return 0;
		} 
	}
	else{
		cout << "Debe ingresar un numero en el parametro 1" << endl;
		cout << "o ingrese un numero dentro de los limites establecidos" << endl;
		return 0;
	}
}

int verificarparametro(int argc, char *argv[]){
	if (argc == 3){
		int N;
		
		try{
			N = stoi(argv[1]);
			verificarTamano(N, argv);
		}
		
		catch (const std::invalid_argument& e){
			cout << "En el primer parametro debe ingresar un numero" << endl;
			return 0;
		}
	}
	else{
		cout << "No ingreso los parametros correctos, reinicie el programa" << endl;
		return 0;
	}
}


int main (int argc, char *argv[]){
	srand(time(0));
	verificarparametro(argc, argv);
	
	return 0;
}

